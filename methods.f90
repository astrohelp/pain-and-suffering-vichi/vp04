    !****************************************************************************
    !
    !  PROGRAM: VP_04
    !
    !  PURPOSE: Модуль, находящий решение системы методами Якоби, Зейделя и релаксации
    !
    !****************************************************************************

module methods
implicit none
real(8), parameter :: accuracy=2e-8

contains

subroutine yakobi(A,B,X,n)
integer(4) :: n, i
real(8), dimension(n) :: X, B, G
real(8), dimension(n,n) :: A, D, D_1, Z
real(8) :: deltaX

    D=0
    D_1=0
    X=1
    
    forall (i=1:n) D(i,i)=A(i,i)
    forall (i=1:n) D_1(i,i)=1/D(i,i)
    Z=matmul((D-A),D_1)
    G=matmul(B,D_1)
    
    deltaX=1
    do while (deltaX > accuracy)
        deltaX=sqrt(sum((matmul(X,Z)+G-X)**2))
        X=matmul(X,Z)+G
    enddo
end subroutine yakobi

subroutine seidel(A,B,X,n)
integer(4) :: n, i, j
real(8), dimension(n) :: X, B, Q, oldX
real(8), dimension(n,n) :: A, P
real(8) :: deltaX
    
    X=0
    
    forall (i=1:n, j=1:n) P(j,i)=-A(j,i)/A(i,i)
    forall (i=1:n) Q(i)=B(i)/A(i,i)

    deltaX=1
    do while (deltaX > accuracy)
        oldX=X
        do i=1,n
	        X(i)=sum(P(1:i-1,i)*X(1:i-1))+sum(P(i+1:n,i)*X(i+1:n))+Q(i)
        enddo
        deltaX=sqrt(sum((X-oldX)**2))
    enddo
end subroutine seidel

subroutine relax(A,B,X,n)
integer(4) :: n, i, j, CoordinateOfMax
real(8), dimension(n) :: X, B, Q
real(8), dimension(n,n) :: A, P
real(8), dimension(n) :: Coordinates
    
    X=0
    
    forall (i=1:n, j=1:n) P(j,i)=-A(j,i)/A(i,i)
    forall (i=1:n) Q(i)=B(i)/A(i,i)

    do while (maxval(abs(Q)) > accuracy)
        CoordinateOfMax=sum(maxloc(abs(Q)))
        
        X(CoordinateOfMax)=X(CoordinateOfMax)+Q(CoordinateOfMax)
        forall (i=1:n) Q(i)=Q(i)+P(CoordinateOfMax,i)*Q(CoordinateOfMax)
    enddo
end subroutine relax


function loss(A,X,B,n)
integer(4) :: n
real(8), dimension(n) :: X, B
real(8), dimension(n,n) :: A
real(8) :: loss
    
    loss = sqrt(sum((matmul(transpose(A),X)-B)**2))
end function loss

subroutine output(n,X,mettype,A,B)
character(6) :: mettype
integer(4) :: n, i
real(8), dimension(n) :: X, B
real(8), dimension(n,n) :: A
    
    if (mettype=='all') then
        open(2,file='result.dat', position = 'append', status='old')
    else
        open(2,file='result.dat',status='replace')
    endif
    write(2,'("# ",i8)') n
    do i=1,n
        write(2,*) X(i)
    enddo
    close(2)
    write(*,*) "модуль вектора невязки ",  loss(A,X,B,n)
end subroutine output
end module methods
